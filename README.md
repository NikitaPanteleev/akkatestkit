Put the file into project directory and rename if needed path in `application.conf`:
```
app {
  dataPath = "sessions_s8_clear.csv"
}
```

Then run `org.happyjail.Boot` as usual scala application. To see actual results, check url like:
```
http://127.0.0.1:9003/v1.0/cities?from=2013-02-11&until=2015-10-12&city=Ber.*
```

Have a nice day.