package org.happyjail

import akka.actor.Actor
import akka.http.scaladsl.model.ContentTypes._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.{RouteTestTimeout, ScalatestRouteTest}
import akka.testkit.TestActorRef
import org.scalatest.{WordSpec, Matchers}

import scala.concurrent.duration._

class ResServiceSpec extends WordSpec with Matchers with ScalatestRouteTest {
  import Protocol._
  implicit val timeout = RouteTestTimeout(3.seconds)

  val actorRef = TestActorRef(new Actor {
    def receive = {
      case _ => sender() ! Response(Some("Ok"))
    }
  })

  val service: RestService = RestService(actorRef)
  val routes: Route = service.routes

  "Service" should {
    "respond with json on /v1.0/cities with valid arguments" in {
      Get("/v1.0/cities?from=2013-02-11&until=2015-10-12&city=Kiel") ~> routes ~> check {
        status shouldBe OK
        contentType shouldBe `application/json`
        responseAs[String] should include (""""message": "Ok"""")
      }
    }

    "respond with error on /v1.0/cities with NOT valid arguments" in {
      Get("/v1.0/cities?from=2013-02-11&until=2015ss-10-12&city=Kiel") ~> routes ~> check {
        status shouldBe StatusCodes.BadRequest
      }
    }
  }
}