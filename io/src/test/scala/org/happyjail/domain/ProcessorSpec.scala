package org.happyjail.domain

import org.happyjail.Protocol._
import org.scalatest.{Matchers, WordSpec}
import com.github.nscala_time.time.Imports._

class ProcessorSpec extends WordSpec with Matchers {
  import Processor._
  val date = DateTime.parse("2013-02-11")
  val data = List(
    Entry(date, "Berlin"),
    Entry(date, "Berlin"),
    Entry(date, "Kiel"),
    Entry(date + 1.month, "Bremen")
  )

  "Processor" should {
    "process data only in given range" in {
      data.query(Query(date, date + 1.day, ".*")) shouldBe List(DateResult(date,
        List(CityResult("Kiel", 1), CityResult("Berlin", 2))))
    }
  }
}
