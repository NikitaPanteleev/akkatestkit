package org.happyjail

import com.github.nscala_time.time.Imports._
import org.happyjail.Protocol.Query
import org.scalatest.{Matchers, WordSpec}

import scala.util.{Failure, Success}

class ProtocolSpec extends WordSpec with Matchers {
  "Protocol" should {
    "fail on NOT valid dates" in {
      Protocol.Query.validate("123ss", "124", "Berlin") shouldBe a [Failure[_]]
    }

    "validate valid dates" in {
      Protocol.Query.validate("2013-02-11", "2015-10-12", "Berlin") shouldBe
        Success(Query(DateTime.parse("2013-02-11"), DateTime.parse("2015-10-12"), "Berlin"))
    }

    "fail on dates with different order" in {
      Protocol.Query.validate("2017-02-11", "2015-10-12", "Berlin") shouldBe a [Failure[_]]
    }
  }
}
