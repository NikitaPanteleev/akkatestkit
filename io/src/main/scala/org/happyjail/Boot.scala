package org.happyjail

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory

object Boot extends App {
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  val config = ConfigFactory.load()
  val recognitionDispatcher: ActorRef = system.actorOf(
    Dispatcher.props(),
    Dispatcher.getClass.getSimpleName)

  val restService: RestService = RestService(recognitionDispatcher)

  val binding = Http().bindAndHandle(
    handler = restService.routes,
    interface = config.getString("http.address"),
    port = config.getInt("http.port")
  )
}
