package org.happyjail

import com.github.nscala_time.time.Imports._

import scala.util.{Failure, Success, Try}

object Protocol {
  sealed trait Command
  case class Query(from: DateTime, until: DateTime, city: String) extends Command
  case class LoadData(path: String) extends Command

  case object DataLoaded
  case class Response(message: Option[String],
    dates: List[DateResult] = Nil
  )
  case class DateResult(date: DateTime, cities: List[CityResult])
  case class CityResult(city: String, count: Int)


  object Query {
    def validate(from: String, until: String, city: String): Try[Query] = {
      for {
        from <- Try { DateTime.parse(from) }
        until <- Try { DateTime.parse(until) }
        _ <- if (from <= until) Success(true) else Failure(new Exception(s"$from greater than $until"))
      } yield  Query(from, until, city)
    }
  }
}
