package org.happyjail

import akka.actor._
import akka.http.scaladsl.model.{HttpRequest, StatusCodes}
import akka.http.scaladsl.server.Directive0
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.directives.{DebuggingDirectives, LoggingMagnet}
import akka.pattern.ask
import akka.stream.ActorMaterializer
import com.typesafe.scalalogging.LazyLogging

import scala.util.{Failure, Success}

case class RestService(dispatcher: ActorRef)
  (implicit system: ActorSystem, mat: ActorMaterializer) extends LazyLogging {

  import Protocol._
  import ProtocolJsonSupport._

  implicit val executionContext = system.dispatcher
  implicit val timeout = Config.timeout

  def logRequest: Directive0 = {
    def log(req: HttpRequest): Unit = {
      logger.debug(
        s"""${ req.headers.find(_.name() == "Host") } - ${ req.uri } - ${ req.entity.toString.replace("\n", " ") }""".stripMargin)
    }
    DebuggingDirectives.logRequest(LoggingMagnet(_ => log))
  }

  val routes = logRequest {
    pathPrefix("v1.0") {
      (get & path("cities")) {
        parameters('from, 'until, 'city) { (from, until, city) =>
          Query.validate(from, until, city) match {
            case Success(query) => complete((dispatcher ? query).mapTo[Response])
            case Failure(ex)    => complete(StatusCodes.BadRequest, s"Get params are not valid: ${ ex.getMessage }")
          }
        }
      }
    }
  }
}

