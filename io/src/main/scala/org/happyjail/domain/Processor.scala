package org.happyjail.domain

import org.happyjail.Protocol.{CityResult, DateResult, Query}
import com.github.nscala_time.time.Imports._

object Processor {
  implicit class ProcessorImpl(data: List[Entry]) {
    def query(query: Query): List[DateResult] = {
      data
        .filter { e =>
          query.from <= e.date && e.date < query.until && e.city.matches(query.city)
        }
        .groupBy(_.date)
        .map {
          case (date, list) =>
            val cities = list.map(_.city).groupBy(identity).mapValues(_.size).map {
              case (city, count) => CityResult(city, count)
            }
            DateResult(date, cities.toList)
        }
        .toList
        .sortBy(_.date)
    }

  }

  def process(iterator: Iterator[String]): List[Entry] = iterator.flatMap(Entry.opt).toList

}
