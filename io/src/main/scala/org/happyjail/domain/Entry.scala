package org.happyjail.domain

import org.joda.time.DateTime

import scala.util.Try

case class Entry(date: DateTime, city: String)

object Entry {
  def opt(s: String): Option[Entry] = Try {
    val parts = s.split(";")
    val date = parts(3)
    val city = parts(10)
    Entry(DateTime.parse(date), city)
  }.toOption
}
