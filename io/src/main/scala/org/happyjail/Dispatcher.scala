package org.happyjail

import akka.actor.{Actor, ActorLogging, Props, Stash}
import akka.pattern.pipe
import org.happyjail.domain.{Entry, Processor}

import scala.concurrent.Future
import scala.io.Source
import scala.util.{Failure, Success}


class Dispatcher() extends Actor with Stash with ActorLogging {
  import Protocol._
  import Processor._
  import context._

  var data = List[Entry]()

  self ! LoadData(Config.path)
  def receive = default

  def default: PartialFunction[Any, Unit] = {
    case LoadData(path) =>
      log.info(s"Loading data for path: $path")
      Future {
        val iterator = Source.fromFile(path).getLines
        data = Processor.process(iterator)
      } onComplete {
        case Success(_) => self ! DataLoaded
        case Failure(ex) =>
          log.error(ex, "Data loading is failed with")
          unstashAll()
          context become dataLoadingFailed
      }
    case DataLoaded =>
      log.info(s"Data is loaded, switching context")
      unstashAll()
      context become dataLoaded
    case msg =>
      log.info(s"Data is not ready, stashing: $msg")
      stash()
  }

  def dataLoaded: PartialFunction[Any, Unit] = {
    case q@Query(from, until, city) =>
      Future {
        val res = data.query(q)
        Response(
          message = None,
          dates = res
        )
      }.pipeTo(sender())
  }

  def dataLoadingFailed: PartialFunction[Any, Unit] = {
    case cmd =>
      sender() ! Response(Some("Data was not read from csv file"))
  }
}

object Dispatcher {
  def props() = Props(classOf[Dispatcher])
}