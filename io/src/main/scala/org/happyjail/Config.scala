package org.happyjail

import akka.util.Timeout
import com.typesafe.config.ConfigFactory

import scala.concurrent.duration._

object Config {
  val config = ConfigFactory.load()

  val httpAddress = config.getString("http.address")
  val httpPort = config.getInt("http.port")

  val timeout = Timeout(60.second)

  val path = config.getString("app.dataPath")
}
