import sbt._

object Dependencies {
  val ScalaTest = "org.scalatest" % "scalatest_2.11" % "2.2.6"
  val MockitoAll = "org.mockito" % "mockito-all" % "1.10.19"
  val ScalaCheck = "org.scalacheck" %% "scalacheck" % "1.13.0" % "test"
  val Logging = "com.typesafe.scala-logging" %% "scala-logging" % "3.4.0"
  val LogBack = "ch.qos.logback" % "logback-classic" % "1.1.3"
  val NScalaTime = "com.github.nscala-time" %% "nscala-time" % "1.8.0"
//  val PlayJson = "com.typesafe.play" %% "play-json" % "2.5.4"
//  val PlayJsonSupport = "octalmind" % "play-json-support_2.11" % "0.2.0"

  object Akka {
    val Version = "2.4.8"
    val Actor = "com.typesafe.akka" %% "akka-actor" % Version
    val Agent = "com.typesafe.akka" %% "akka-agent" % Version
    val Remote = "com.typesafe.akka" %% "akka-remote" % Version % "compile"
    val Slf4j = "com.typesafe.akka" %% "akka-slf4j" % Version % "compile"
    val Persistence = "com.typesafe.akka" %% "akka-persistence" % Version % "compile"
    val Cluster = "com.typesafe.akka" %% "akka-cluster" % Version % "compile"
    val ClusterSharding = "com.typesafe.akka" %% "akka-cluster-sharding" % Version % "compile"
    val ClusterTools = "com.typesafe.akka" %% "akka-cluster-tools" % Version % "compile"
    val DistributedData = "com.typesafe.akka" %% "akka-distributed-data-experimental" % Version % "compile"
    val Contrib = "com.typesafe.akka" %% "akka-contrib" % Version % "compile"
    val Testkit = "com.typesafe.akka" %% "akka-testkit" % Version % "compile"
    val Stream = "com.typesafe.akka" %% "akka-stream" % Version
    val StreamTesktit = "com.typesafe.akka" %% "akka-stream-testkit" % Version % "test"
    val HttpCore = "com.typesafe.akka" %% "akka-http-core" % Version
    val HttpExperimental = "com.typesafe.akka" %% "akka-http-experimental" % Version
    val HttpTestKit = "com.typesafe.akka" %% "akka-http-testkit" % Version % "test"
    val HttpSprayJson = "com.typesafe.akka" %% "akka-http-spray-json-experimental" % Version


    val All: Seq[ModuleID] = Seq(Actor, Agent, Remote, Slf4j, Persistence, Testkit,
      Cluster, ClusterSharding, ClusterTools, DistributedData, Contrib, Stream,
      StreamTesktit, HttpCore, HttpExperimental, HttpTestKit, HttpSprayJson)
  }
}