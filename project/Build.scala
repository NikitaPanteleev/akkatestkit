import Dependencies._
import sbt.Keys._
import sbt._

object Build extends Build {
  lazy val basicSettings = Seq(
    version := "1.0",
    scalaVersion := "2.11.8",
    libraryDependencies := Seq(NScalaTime, ScalaTest, ScalaCheck, MockitoAll),
    resolvers += "octalmind maven" at "http://dl.bintray.com/guillaumebreton/maven"
  )

//  val pure = Project("pure", file("pure"))
//    .settings (basicSettings: _*)
//    .settings (libraryDependencies ++= Seq())

  val io = Project("io", file("io"))
//    .dependsOn(pure)
    .settings (basicSettings: _*)
    .settings (libraryDependencies ++= Seq(Logging, LogBack) ++ Akka.All)

//  val apiTest = Project("test", file("test"))
//    .settings (basicSettings: _*)
//    .settings (libraryDependencies ++= Akka.All)
}